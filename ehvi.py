#! /usr/bin/env python
"""
Hypervolume expected Improvement calculator module
"""

__author__ = "Anjana Talapatra (anjanatalapatra@tamu.edu)"
__version__ = "0.1$"
__date__ = "$Date: Jan 2017 $"
__copyright__ = "Copyright (c) 2017 Anjana Talapatra"
__license__ = "Python"

#----------------------------------------------------------------------------------------------------%
from numpy import sqrt, pi, exp, sum, inf, zeros
from math import erf
#----------------------------------------------------------------------------------------------------%
def gausspdf(x):
    res = 1/sqrt(2*pi)*exp(-x**2/2.0)
    return res
#----------------------------------------------------------------------------------------------------%
def gausscdf(x):
    res = y=0.5*(1+erf(x/sqrt(2.0)))
    return res
#----------------------------------------------------------------------------------------------------%
def hvolume2d(P, x): # sorted P
    h=0
    S = sorted(P[i] for i in range(len(P)))
    if (not(len(P))==0):
        k=len(P)
        for i in range(0,k):
            if(i==0):
                h= h + (x[0]-S[i][0])*(x[1]-S[i][1])
            else:
                h= h + (x[0]-S[i][0])*(S[i-1][1]-S[i][1])
    return h
#----------------------------------------------------------------------------------------------------%	
def exipsi(a,b,m,s):

    x = s*gausspdf((b-m)/float(s)) + (a-m)*gausscdf((b-m)/float(s))
    return x
#----------------------------------------------------------------------------------------------------%
def exi2d(S,r,mu,s):
    k=len(S)
    c1 = sorted([S[i][0] for i in range(k)])
    c2 = sorted([S[i][1] for i in range(k)])
    c = zeros((k+1,k+1))

    for i in range(-1,k):
        for j in range(-1,k-1-i):
            if (j==-1):
                fMax2=r[1]
            else:
                fMax2 = c2[k-j-1]
            if (i==-1):
                fMax1=r[0] 
            else:
                fMax1 = c1[k-i-1]
            if (j==-1): 
                cL1= -inf
            else: 
                cL1 = c1[j]
            if (i==-1): 
                cL2= -inf 
            else: 
                cL2 = c2[i]
            if (j==k-1): 
                cU1 = r[0]
            else:
                cU1 = c1[j+1]
            if (i==k-1): 
                cU2 = r[1] 
            else: 
                cU2 = c2[i+1]
            SM=[]
            for m in range(0,k):
                if (cU1 <= S[m][0] and cU2 <= S[m][1]):
                    SM.insert(0,[S[m][0],S[m][1]])
            #print fMax1, fMax2, cL1, cL2, cU1, cU2, SM
            sPlus = hvolume2d(SM, [fMax1,fMax2])
            Psi1 = exipsi(fMax1,cU1,mu[0],s[0]) - exipsi(fMax1,cL1,mu[0],s[0])
            Psi2 = exipsi(fMax2,cU2,mu[1],s[1]) - exipsi(fMax2,cL2,mu[1],s[1])

            GaussCDF1 = gausscdf((cU1-mu[0])/float(s[0])) - gausscdf((cL1-mu[0])/float(s[0]))
        
            GaussCDF2 = gausscdf((cU2-mu[1])/float(s[1])) - gausscdf((cL2-mu[1])/float(s[1]))
            c[i+1][j+1]= Psi1*Psi2-sPlus*GaussCDF1*GaussCDF2
            #print sPlus, Psi1, Psi2, GaussCDF1, GaussCDF2, c[i+1][j+1]


    res=sum(c)
    return res
    #----------------------------------------------------------------------------------------------------%














