#! /usr/bin/env python
"""
Gaussian Predictor Module  for EGO
"""

__author__ = "Anjana Talapatra (anjanatalapatra@tamu.edu),Thien Duong, Shahin Boluki "
__version__ = "0.1$"
__date__ = "$Date: May 2016 $"
__copyright__ = "Copyright (c) 2016 Anjana Talapatra, Thien Duong, Shahin Boluki"
__license__ = "Python"

#----------------------------------------------------------------------------------------------------%
import os
import json
import tammal.optimal_predictor.ego_setup as setup
import numpy as np
import scipy as sp
from operator import itemgetter
import time
import json
import pymatgen.serializers.json_coders as json_coders
from sklearn.gaussian_process import GaussianProcess
import sklearn.preprocessing as pre
from tammal.optimal_predictor.ehvi import exi2d
import random
import copy
#----------------------------------------------------------------------------------------------------%
def gaussian_predictor(materials, target_property, feature_set, availExpCount, excludedList,params_vasp,criteria, gp_type, input_list, input_set,feature_set_no):

    # define calculation parameters
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    orig_materials = materials
    #----------------------------------------------------------------------------------------------------%
    priorKnowledge, availableExperiments, gpSamples = setup.setup_gp(materials,availExpCount, target_property,feature_set,excludedList)
    gp = setup.initialize_gp(availableExperiments,gp_type)
    #print availableExperiments
    predOptProps, sigma, EI = setup.evaluate_EI(gp, priorKnowledge, availableExperiments, gpSamples, criteria)
    calc_K = [x['K_exp'] for x in materials]
#    print predOptProps-calc_K
    #----------------------------------------------------------------------------------------------------%
    maxEI = np.amax(EI) * np.ones(maxEICheck)
    it = 0
    numIter = 40
    output_list=[]
    if criteria=='max':
    	max_output = max(availableExperiments)
	min_output = 310
    elif criteria =='min':
	min_output = min(availableExperiments)
	max_output = 200
	
    while not stopEILoop and it < numIter and max_output < 300:#and len(excludedList) < len(materials) and max_output < 300 and min_output > 11:
        it+=1
	#print it
        materials = sorted(materials,key=itemgetter('Index'))
        for i in range(len(materials)):
            materials[i]['EI'] = EI[i]
            materials[i]['Mean'] = predOptProps[i]
            materials[i]['Variance'] = sigma[i]
        # Fitting GP
        predOptProps, sigma, EI = setup.evaluate_EI(gp, priorKnowledge, availableExperiments,gpSamples,criteria)
#	print predOptProps.shape
        maxEI = np.roll(maxEI, -1)
        maxEI[-1] = np.amax(EI)

        # Assign GP predictions to materials properties
        for j in range(len(materials)):
            materials[j]['EI'] = EI[j]
            materials[j]['Predicted_output'] = predOptProps[j]
            materials[j]['Variance'] = sigma[j]

        candidates = sorted(materials,key=itemgetter('EI'),reverse=True)
        candidate_Index = []
        m = 0
        iStop = 0    
        while(iStop < num_candidates and m < len(materials) and not stopEILoop):
           currentFeatures = np.atleast_2d(gpSamples[candidates[m]['Index']-1, :])
           if candidates[m]['Index'] not in excludedList:
                candidate_Index = (candidates[m]['Index'])
                priorKnowledge = np.concatenate((priorKnowledge, currentFeatures), axis=0)
                list_data =[candidate_Index]
                if criteria =='min':
                        materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['G_exp']
                elif criteria =='max':
                        materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['K_exp']
                availableExperiments = np.append(availableExperiments,materials[candidate_Index-1][target_property[0]])
                excludedList = np.append(excludedList, candidate_Index)
                candidate_material = candidates[m]['MAX']
                iStop+=1
           m+=1
        time.sleep(2)
        calc_K = [x['K_exp'] for x in materials]
        calc_G = [x['G'] for x in materials]
	if criteria=='max':
        	max_output = max(availableExperiments)
        	min_output = 310
    	elif criteria =='min':
        	min_output = min(availableExperiments)
        	max_output = 200
        #max_K = max(calc_K)
        #min_G = min(availableExperiments)
        ego_run_data = dict()
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EI':np.max(EI)})
        ego_run_data.update({'max_Bulk_Calculated':max_output})
	ego_run_data.update({'min_G_Calculated':min_output})
        output_list.append(ego_run_data)
	#if it==45 or it==95 or it==145 or it==195:
	#	final_prediction = []
	#	final_prediction.append((predOptProps+sigma).tolist())
	#	json_coders.json_pretty_dump(final_prediction,'final_prediction_{}_{}_{}_{}.json'.format(str(input_list), str(input_set),str(feature_set_no),str(it)))
    materials = sorted(materials,key=itemgetter('Index'))
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set),str(feature_set_no)))
#    predOptProps, sigma, EI = setup.evaluate_EI(gp, priorKnowledge, availableExperiments,gpSamples,criteria)
    #print availableExperiments
    #print it
    #final_prediction = []
    #final_prediction.append((predOptProps-calc_K).tolist())
    #json_coders.json_pretty_dump(final_prediction,'final_prediction.json')
#----------------------------------------------------------------------------------------------------%

def multi_objective_ehvi(materials, target_property, feature_set, feature_set1,availExpCount, excludedList,gp_type,input_list, input_set,feature_set_no):
    # define calculation parameters
    #stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters_direct()
    orig_materials = materials
    #----------------------------------------------------------------------------------------------------%

    n_mat = len(materials)
    predOptProp = np.zeros((len(target_property),n_mat,1))
    sigma = np.zeros((len(target_property),n_mat,1))
    EI = np.zeros((len(target_property),n_mat,1))
    criteria = [ 'max','min']  
    priorKnowledge0, availableExperiments0, gpSamples0 = setup.setup_gp(materials,availExpCount, target_property[0] ,feature_set,excludedList)
    priorKnowledge1, availableExperiments1, gpSamples1 = setup.setup_gp(materials,availExpCount, target_property[1] ,feature_set1,excludedList)
    gp0 = setup.initialize_gp(availableExperiments0,gp_type)
    gp1 = setup.initialize_gp(availableExperiments1,gp_type)
    predOptProp0, sigma0, EI0 = setup.evaluate_EI(gp0, priorKnowledge0, availableExperiments0, gpSamples0,criteria[0])
    predOptProp1, sigma1, EI1 = setup.evaluate_EI(gp1, priorKnowledge1, availableExperiments1, gpSamples1,criteria[1])
    initial_K = availableExperiments0
    initial_G =availableExperiments1
    #----------------------------------------------------------------------------------------------------%
    
   
    materials = sorted(materials,key=itemgetter('Index'))
    for i in range(n_mat):
            materials[i]['EI_1'] = EI0[i]
            materials[i]['Mean_1'] = predOptProp0[i]
            materials[i]['Variance_1'] = sigma0[i]
            materials[i]['EI_2'] = EI1[i]
            materials[i]['Mean_2'] = predOptProp1[i]
            materials[i]['Variance_2'] = sigma1[i]
    numIter = 40
    it = 0
    output_list=[]

    D_max =  np.amax( np.divide(availableExperiments0,availableExperiments1))
    
    availableExperimentsAll0 = np.zeros((len(materials),1))
    availableExperimentsAll1 = np.zeros((len(materials),1))
    for iiii in range(len(materials)):
        availableExperimentsAll0[iiii] = materials[iiii]['K_exp']
        availableExperimentsAll1[iiii] = materials[iiii]['G_exp']
    
    Pareto_All_1, Pareto_All_2, True_Pareto_list = pareto_frontier(availableExperimentsAll0, availableExperimentsAll1,maxX = True, maxY = False)
    Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))
    while not stopEILoop and it < numIter and len(excludedList) < len(materials) and len(Pareto_points_found) < len(True_Pareto_list): 
        it+=1

        predOptProp0, sigma0, EI0 = setup.evaluate_EI(gp0, priorKnowledge0, availableExperiments0, gpSamples0, criteria[0])
        predOptProp1, sigma1, EI1 = setup.evaluate_EI(gp1, priorKnowledge1, availableExperiments1, gpSamples1, criteria[1])

        
                # Assign GP predictions to materials properties
        for i in range(n_mat):
            materials[i]['EI_1'] = EI0[i]
            materials[i]['Mean_1'] = predOptProp0[i]
            materials[i]['Variance_1'] = sigma0[i]
            materials[i]['EI_2'] = EI1[i]
            materials[i]['Mean_2'] = predOptProp1[i]
            materials[i]['Variance_2'] = sigma1[i]
        
        candidate_P_1, candidate_P_2, new_candidate_list = pareto_frontier(availableExperiments0, availableExperiments1,maxX = True, maxY = False)
        k = len(candidate_P_1)
        if it==1:
            Pareto_K = [candidate_P_1[i][0] for i in range(k)]
            Pareto_G = [candidate_P_2[i][0] for i in range(k)]
        else:
            Pareto_K = candidate_P_1
            Pareto_G = candidate_P_2
        
        S = sorted([-candidate_P_1[i], candidate_P_2[i]] for i in range(k))
        #r = [-301,1]
        r = [-50,200]

        ehvi = np.zeros(n_mat)
        for i in range(n_mat):
            mu = [-predOptProp0[i], predOptProp1[i]]
            s = [sigma0[i],sigma1[i]]
            ehvi[i] = exi2d(S,r,mu,s)
            materials[i]['EHVI'] = ehvi[i]

        # Assign EHVI to materials properties
                
        candidates = sorted(materials,key=itemgetter('EHVI'),reverse=True)
        index_list=[]
        for i in range(0,n_mat):
            index_list.append(candidates[i]['Index'])
        exclusion_list =[]
        for i in range(0,len(excludedList)):
            exclusion_list.append(excludedList[i])
        
        for l in index_list[:]:
            if l in exclusion_list:
                index_list.remove(l)
        ncandidates = 2
        candidate_index = index_list[:ncandidates]
        for i in range(0,len(candidate_index)):           
            n = int(candidate_index[i])
            currentFeatures0 = np.atleast_2d(gpSamples0[n-1, :])
            currentFeatures1 = np.atleast_2d(gpSamples1[n-1, :])
            priorKnowledge0 = np.concatenate((priorKnowledge0, currentFeatures0))
            priorKnowledge1 = np.concatenate((priorKnowledge1, currentFeatures1))
            
            list_data =[n] 
            materials[n-1]['K'] = materials[n-1]['K_exp']
            materials[n-1]['G'] = materials[n-1]['G_exp']
            materials[n-1]['E'] = materials[n-1]['E_exp']
            new_property = np.zeros((len(target_property),1, 1))
            new_property0 = materials[n-1]['K']
            new_property1 = materials[n-1]['G']
            availableExperiments0 = np.append(availableExperiments0,new_property0)
            availableExperiments1 = np.append(availableExperiments1,new_property1)

            excludedList = np.append(excludedList, n)

            candidate_material = materials[n-1]['MAX']
            time.sleep(2)
            calc_K = [x['K'] for x in materials]
            calc_G = [x['G'] for x in materials]

            ductility_index_calc = np.ones(len(calc_K))
            pred_D = predOptProp0*predOptProp1
  
        for x in range(len(calc_G)):
           if calc_G[x] ==0:
               ductility_index_calc[x] = 0
           else:
                ductility_index_calc[x] = calc_K[x]/calc_G[x]
        Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))
        D_max =  np.amax(ductility_index_calc)       
        Max_Index = materials[np.argmax(ductility_index_calc)]['Index']
        Max_MAX = materials[np.argmax(ductility_index_calc)]['MAX']
        K_max = materials[np.argmax(ductility_index_calc)]['K']
        G_min = materials[np.argmax(ductility_index_calc)]['G']

        ego_run_data = dict()
        ego_run_data.update({'initial_G':np.asarray(initial_G).tolist()})
        ego_run_data.update({'initial_K':np.asarray(initial_K).tolist()})
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EHVI':np.amax(ehvi)})
        ego_run_data.update({'Max_D_calc':np.amax(ductility_index_calc)})
        ego_run_data.update({'Number_ParetoPoints_Found':len(Pareto_points_found)})
        output_list.append(ego_run_data)
    materials = sorted(materials,key=itemgetter('Index'))    
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set),str(feature_set_no)))

#-------------------------------------------------------------------------------------------
def gaussian_predictor_multi_objective_averaging_first(materials, target_property, feature_set, feature_set1,availExpCount, excludedList,gp_type,input_list, input_set,feature_type):
    # define calculation parameters
    #stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters_direct()
    orig_materials = materials
    #----------------------------------------------------------------------------------------------------%

    n_mat = len(materials)
    
    availableExperimentsAll0 = np.zeros((len(materials),1))
    availableExperimentsAll1 = np.zeros((len(materials),1))
    for iiii in range(len(materials)):
        availableExperimentsAll0[iiii] = materials[iiii]['K_exp']
        availableExperimentsAll1[iiii] = materials[iiii]['G_exp']
        
    Pareto_All_1, Pareto_All_2, True_Pareto_list = pareto_frontier(availableExperimentsAll0, availableExperimentsAll1,maxX = True, maxY = False)
    
    priorKnowledge0 =np.zeros((len(feature_set),availExpCount,4))
    priorKnowledge1 =np.zeros((len(feature_set1),availExpCount,4))
    gpSamples0=np.zeros((len(feature_set),n_mat,4))
    gpSamples1=np.zeros((len(feature_set1),n_mat,4))
    
    for i in range(len(feature_set)):
        #priorKnowledge[i], availableExperiments, gpSamples[i] = setup.setup_gp(materials,availExpCount, target_property,feature_set[i],excludedList)
        priorKnowledge0[i], availableExperiments0, gpSamples0[i] = setup.setup_gp(materials,availExpCount, target_property[0] ,feature_set[i],excludedList)
        priorKnowledge1[i], availableExperiments1, gpSamples1[i] = setup.setup_gp(materials,availExpCount, target_property[1] ,feature_set1[i],excludedList)
    
    predOptProp0 = np.zeros((len(feature_set),n_mat))
    predOptProp1 = np.zeros((len(feature_set),n_mat))
    sigma0 = np.zeros((len(feature_set),n_mat))
    sigma1 = np.zeros((len(feature_set),n_mat))
    EI0 = np.zeros((len(feature_set),n_mat))
    EI1 = np.zeros((len(feature_set),n_mat))
    criteria = [ 'max','min']
    
    RML =np.zeros(len(feature_set))
    RML0 =np.zeros(len(feature_set))
    RML1 =np.zeros(len(feature_set))
    RML_sum = 0
    C0 = np.zeros((len(feature_set), availExpCount,availExpCount))
    C1 = np.zeros((len(feature_set), availExpCount,availExpCount))
    theta0 = np.zeros((len(feature_set),1))
    theta1 = np.zeros((len(feature_set),1))
    sigma20 = np.zeros((len(feature_set),1))
    sigma21 = np.zeros((len(feature_set),1))
    beta0 = np.zeros((len(feature_set),1))
    beta1 = np.zeros((len(feature_set),1))
    nugget0 = np.zeros((len(feature_set),1))
    nugget1 = np.zeros((len(feature_set),1))
    gp0 = setup.initialize_gp(availableExperiments0,gp_type)
    gp1 = setup.initialize_gp(availableExperiments1,gp_type)
    
    for i in range(len(feature_set)):
        RML0[i],C0[i],theta0[i], sigma20[i], predOptProp0[i], sigma0[i], EI0[i], beta0[i], nugget0[i] = setup.evaluate_EI_averaging(gp0, priorKnowledge0[i], availableExperiments0, gpSamples0[i], criteria[0])
        RML1[i],C1[i],theta1[i], sigma21[i], predOptProp1[i], sigma1[i], EI1[i], beta1[i], nugget1[i] = setup.evaluate_EI_averaging(gp1, priorKnowledge1[i], availableExperiments1, gpSamples1[i], criteria[1])
        dummy_Y0 = availableExperiments0-((np.std(availableExperiments0)*beta0[i])+np.mean(availableExperiments0))
        dummy_Y1 = availableExperiments1-((np.std(availableExperiments1)*beta1[i])+np.mean(availableExperiments1))
        RML0_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
        RML1_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
        if RML0_tmp != np.inf:
            RML0[i] = RML0_tmp
        if RML1_tmp != np.inf:
            RML1[i] = RML1_tmp
        RML[i]=RML0[i]+RML1[i]
        RML_sum = RML_sum+RML[i]
    eff_coeff =RML
    min_coeff =max(eff_coeff)
    log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
    coeffs = np.exp(eff_coeff - log_sum)
    EI_effective0= calculate_EI_effective(n_mat,EI0,coeffs)
    EI_effective1= calculate_EI_effective(n_mat,EI1,coeffs)
    Mean_0_effective0= calculate_EI_effective(n_mat,predOptProp0,coeffs)
    Mean_1_effective1= calculate_EI_effective(n_mat,predOptProp1,coeffs)
    
    #Just to initialize
    EHVI_effective= calculate_EI_effective(n_mat,EI1,coeffs)

    initial_K = availableExperiments0
    initial_G =availableExperiments1
    #----------------------------------------------------------------------------------------------------%
    
   
    materials = sorted(materials,key=itemgetter('Index'))
    for i in range(n_mat):
            materials[i]['EI_1'] = EI_effective0[i]
            materials[i]['Mean_1'] = Mean_0_effective0[i]
            materials[i]['EI_2'] = EI_effective1[i]
            materials[i]['Mean_2'] = Mean_1_effective1[i]
    numIter = 40
    it = 0
    output_list=[]

    D_max =  np.amax( np.divide(availableExperiments0,availableExperiments1))
    
    Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))

    #while not stopEILoop and it < numIter and len(excludedList) < len(materials) and D_max < 8:
    while not stopEILoop and it < numIter and len(excludedList) < len(materials) and len(Pareto_points_found) < len(True_Pareto_list):    
        it+=1
        
        C0 = np.zeros((len(feature_set), len(availableExperiments0),len(availableExperiments0)))
        C1 = np.zeros((len(feature_set), len(availableExperiments1),len(availableExperiments1)))

        for i in range(len(feature_set)):
            RML0[i],C0[i],theta0[i], sigma20[i], predOptProp0[i], sigma0[i], EI0[i], beta0[i],nugget0[i] = setup.evaluate_EI_averaging(gp0, priorKnowledge0[i], availableExperiments0, gpSamples0[i], criteria[0])
            RML1[i],C1[i],theta1[i], sigma21[i], predOptProp1[i], sigma1[i], EI1[i], beta1[i],nugget1[i] = setup.evaluate_EI_averaging(gp1, priorKnowledge1[i], availableExperiments1, gpSamples1[i], criteria[1])
            dummy_Y0 = availableExperiments0.reshape(len(availableExperiments0),1)-((np.std(availableExperiments0.reshape(1,-1))*beta0[i])+np.mean(availableExperiments0.reshape(1,-1)))
            dummy_Y1 = availableExperiments1.reshape(len(availableExperiments0),1)-((np.std(availableExperiments1.reshape(1,-1))*beta1[i])+np.mean(availableExperiments1.reshape(1,-1)))

            RML0_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
            RML1_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
            if RML0_tmp != np.inf:
                RML0[i] = RML0_tmp
            if RML1_tmp != np.inf:
                RML1[i] = RML1_tmp
            RML[i]=RML0[i]+RML1[i]
            RML_sum = RML_sum+RML[i]
            
        eff_coeff =RML
        min_coeff =max(eff_coeff)
        log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
        coeffs = np.exp(eff_coeff - log_sum)
        EI_effective0= calculate_EI_effective(n_mat,EI0,coeffs)
        EI_effective1= calculate_EI_effective(n_mat,EI1,coeffs)
        
        Mean_0_effective0= calculate_EI_effective(n_mat,predOptProp0,coeffs)
        Mean_1_effective1= calculate_EI_effective(n_mat,predOptProp1,coeffs)

        
                # Assign GP predictions to materials properties
        for i in range(n_mat):
            materials[i]['EI_1'] = EI_effective0[i]
            materials[i]['Mean_1'] = Mean_0_effective0[i]
            materials[i]['EI_2'] = EI_effective1[i]
            materials[i]['Mean_2'] = Mean_1_effective1[i]
        
        candidate_P_1, candidate_P_2, new_candidate_list = pareto_frontier(availableExperiments0, availableExperiments1,maxX = True, maxY = False)
        k = len(candidate_P_1)
        if it==1:
            Pareto_K = [candidate_P_1[i][0] for i in range(k)]
            Pareto_G = [candidate_P_2[i][0] for i in range(k)]
        else:
            Pareto_K = candidate_P_1
            Pareto_G = candidate_P_2
        
        S = sorted([-candidate_P_1[i], candidate_P_2[i]] for i in range(k))
        r = [-50,200]

        ehvi = np.zeros((len(feature_set),n_mat))
        
        for iii in range(len(feature_set)):
            for i in range(n_mat):
                mu = [-predOptProp0[iii][i], predOptProp1[iii][i]]
                s = [sigma0[iii][i],sigma1[iii][i]]
                ehvi[iii][i] = exi2d(S,r,mu,s)
        
        EHVI_effective= calculate_EI_effective(n_mat,ehvi,coeffs)
        for i in range(n_mat):
            materials[i]['EHVI'] = EHVI_effective[i]

        # Assign EHVI to materials properties
                
        candidates = sorted(materials,key=itemgetter('EHVI'),reverse=True)
        index_list=[]
        for i in range(0,n_mat):
            index_list.append(candidates[i]['Index'])
        exclusion_list =[]
        for i in range(0,len(excludedList)):
            exclusion_list.append(excludedList[i])
        
        
        
        for l in index_list[:]:
            if l in exclusion_list:
                index_list.remove(l)
        ncandidates = 2
        candidate_index = index_list[:ncandidates]
        
        for i in range(0,len(candidate_index)):
            n = int(candidate_index[i])
            excludedList = np.append(excludedList, n)
            updated_prior0 = np.zeros((len(feature_set),len(excludedList),4))
            updated_prior1 = np.zeros((len(feature_set1),len(excludedList),4))
            for iiiii in range(len(feature_set)):
            
                currentFeatures0 = np.atleast_2d(gpSamples0[iiiii, n-1, :])
                currentFeatures1 = np.atleast_2d(gpSamples1[iiiii, n-1, :])
                a0 = np.insert(priorKnowledge0[iiiii], len(priorKnowledge0[iiiii]),currentFeatures0, axis=0)
                a1 = np.insert(priorKnowledge1[iiiii], len(priorKnowledge1[iiiii]),currentFeatures1, axis=0)
                updated_prior0[iiiii] = a0
                updated_prior1[iiiii] = a1
            
            priorKnowledge0 = updated_prior0
            priorKnowledge1 = updated_prior1
            list_data =[n] 
            materials[n-1]['K'] = materials[n-1]['K_exp']
            materials[n-1]['G'] = materials[n-1]['G_exp']
            materials[n-1]['E'] = materials[n-1]['E_exp']
            new_property = np.zeros((len(target_property),1, 1))
            new_property0 = materials[n-1]['K']
            new_property1 = materials[n-1]['G']
            availableExperiments0 = np.append(availableExperiments0,new_property0)
            availableExperiments1 = np.append(availableExperiments1,new_property1)

            candidate_material = materials[n-1]['MAX']
            time.sleep(2)
            calc_K = [x['K'] for x in materials]
            calc_G = [x['G'] for x in materials]

            ductility_index_calc = np.ones(len(calc_K))
            pred_D = predOptProp0*predOptProp1
            
        Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))
  
        for x in range(len(calc_G)):
           if calc_G[x] ==0:
               ductility_index_calc[x] = 0
           else:
                ductility_index_calc[x] = calc_K[x]/calc_G[x]
        D_max =  np.amax(ductility_index_calc)       
        Max_Index = materials[np.argmax(ductility_index_calc)]['Index']
        Max_MAX = materials[np.argmax(ductility_index_calc)]['MAX']
        K_max = materials[np.argmax(ductility_index_calc)]['K']
        G_min = materials[np.argmax(ductility_index_calc)]['G']

        ego_run_data = dict()
        ego_run_data.update({'initial_G':np.asarray(initial_G).tolist()})
        ego_run_data.update({'initial_K':np.asarray(initial_K).tolist()})
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EHVI':np.amax(ehvi)})
        ego_run_data.update({'Max_D_calc':np.amax(ductility_index_calc)})
        ego_run_data.update({'Number_ParetoPoints_Found':len(Pareto_points_found)})
	ego_run_data.update({'L_coeff':coeffs.tolist()})
        #print(len(Pareto_points_found))
        #print(np.shape(priorKnowledge0))
        time.sleep(2)
        output_list.append(ego_run_data)
    materials = sorted(materials,key=itemgetter('Index'))    
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set),str(feature_type)))    
    
#----------------------------------------------------------------------------------------------------
def gaussian_predictor_multi_objective_averaging_second(materials, target_property, feature_set, feature_set1,availExpCount, excludedList,gp_type,input_list, input_set,feature_type):
    # define calculation parameters
    #stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters_direct()
    orig_materials = materials
    #----------------------------------------------------------------------------------------------------%

    n_mat = len(materials)
    
    availableExperimentsAll0 = np.zeros((len(materials),1))
    availableExperimentsAll1 = np.zeros((len(materials),1))
    for iiii in range(len(materials)):
        availableExperimentsAll0[iiii] = materials[iiii]['K_exp']
        availableExperimentsAll1[iiii] = materials[iiii]['G_exp']
        
    Pareto_All_1, Pareto_All_2, True_Pareto_list = pareto_frontier(availableExperimentsAll0, availableExperimentsAll1,maxX = True, maxY = False)
    
    priorKnowledge0 =np.zeros((len(feature_set),availExpCount,4))
    priorKnowledge1 =np.zeros((len(feature_set1),availExpCount,4))
    gpSamples0=np.zeros((len(feature_set),n_mat,4))
    gpSamples1=np.zeros((len(feature_set1),n_mat,4))
    
    for i in range(len(feature_set)):
        priorKnowledge0[i], availableExperiments0, gpSamples0[i] = setup.setup_gp(materials,availExpCount, target_property[0] ,feature_set[i],excludedList)
        priorKnowledge1[i], availableExperiments1, gpSamples1[i] = setup.setup_gp(materials,availExpCount, target_property[1] ,feature_set1[i],excludedList)
    
    predOptProp0 = np.zeros((len(feature_set),n_mat))
    predOptProp1 = np.zeros((len(feature_set),n_mat))
    sigma0 = np.zeros((len(feature_set),n_mat))
    sigma1 = np.zeros((len(feature_set),n_mat))
    EI0 = np.zeros((len(feature_set),n_mat))
    EI1 = np.zeros((len(feature_set),n_mat))
    criteria = [ 'max','min']
    
    RML =np.zeros(len(feature_set))
    RML0 =np.zeros(len(feature_set))
    RML1 =np.zeros(len(feature_set))
    RML_sum = 0
    hessian_det0 = np.zeros((len(feature_set),2,2))
    hessian_det1 = np.zeros((len(feature_set),2,2))
    lap_coeff0 = np.zeros(len(feature_set))
    lap_coeff1 = np.zeros(len(feature_set))
    lap_coeff = np.zeros(len(feature_set))
    C0 = np.zeros((len(feature_set), availExpCount,availExpCount))
    C1 = np.zeros((len(feature_set), availExpCount,availExpCount))
    theta0 = np.zeros((len(feature_set),1))
    theta1 = np.zeros((len(feature_set),1))
    sigma20 = np.zeros((len(feature_set),1))
    sigma21 = np.zeros((len(feature_set),1))
    beta0 = np.zeros((len(feature_set),1))
    beta1 = np.zeros((len(feature_set),1))
    nugget0 = np.zeros((len(feature_set),1))
    nugget1 = np.zeros((len(feature_set),1))
    gp0 = setup.initialize_gp(availableExperiments0,gp_type)
    gp1 = setup.initialize_gp(availableExperiments1,gp_type)
    
    for i in range(len(feature_set)):
        RML0[i],C0[i],theta0[i], sigma20[i], predOptProp0[i], sigma0[i], EI0[i], beta0[i],nugget0[i] = setup.evaluate_EI_averaging(gp0, priorKnowledge0[i], availableExperiments0, gpSamples0[i], criteria[0])
        RML1[i],C1[i],theta1[i], sigma21[i], predOptProp1[i], sigma1[i], EI1[i], beta1[i],nugget1[i] = setup.evaluate_EI_averaging(gp1, priorKnowledge1[i], availableExperiments1, gpSamples1[i], criteria[1])
        

        dummy_Y0 = availableExperiments0-((np.std(availableExperiments0)*beta0[i])+np.mean(availableExperiments0))
        hessian_det0[i] = setup.evaluate_hessian_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
        std_target = np.std(availableExperiments0)
        lap_coeff0[i] = setup.evaluate_laplace_coefficient(hessian_det0[i],theta0[i],np.sqrt(sigma20[i]), std_target)
        dummy_Y1 = availableExperiments1-((np.std(availableExperiments1)*beta1[i])+np.mean(availableExperiments1))
        hessian_det1[i] = setup.evaluate_hessian_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
        std_target = np.std(availableExperiments1)
        lap_coeff1[i] = setup.evaluate_laplace_coefficient(hessian_det1[i],theta1[i],np.sqrt(sigma21[i]), std_target)
        lap_coeff[i] = lap_coeff0[i] + lap_coeff1[i]
        RML0_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
        RML1_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
        if RML0_tmp != np.inf:
            RML0[i] = RML0_tmp
        if RML1_tmp != np.inf:
            RML1[i] = RML1_tmp
        RML[i]=RML0[i]+RML1[i]
        RML_sum = RML_sum+RML[i]
    eff_coeff =RML + lap_coeff
    min_coeff =max(eff_coeff)
    log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
    coeffs = np.exp(eff_coeff - log_sum)
    EI_effective0= calculate_EI_effective(n_mat,EI0,coeffs)
    EI_effective1= calculate_EI_effective(n_mat,EI1,coeffs)
    Mean_0_effective0= calculate_EI_effective(n_mat,predOptProp0,coeffs)
    Mean_1_effective1= calculate_EI_effective(n_mat,predOptProp1,coeffs)
    
    #Just to initialize
    EHVI_effective= calculate_EI_effective(n_mat,EI1,coeffs)

    initial_K = availableExperiments0
    initial_G =availableExperiments1
    #----------------------------------------------------------------------------------------------------%
    
   
    materials = sorted(materials,key=itemgetter('Index'))
    for i in range(n_mat):
            materials[i]['EI_1'] = EI_effective0[i]
            materials[i]['Mean_1'] = Mean_0_effective0[i]
         #   materials[i]['Variance_1'] = sigma0[i]
            materials[i]['EI_2'] = EI_effective1[i]
            materials[i]['Mean_2'] = Mean_1_effective1[i]
         #   materials[i]['Variance_2'] = sigma1[i]
    numIter = 40
    it = 0
    output_list=[]

    D_max =  np.amax( np.divide(availableExperiments0,availableExperiments1))
    
    Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))

    while not stopEILoop and it < numIter and len(excludedList) < len(materials) and len(Pareto_points_found) < len(True_Pareto_list):    
        it+=1
        old_lap_coeff = copy.deepcopy(lap_coeff)
        old_RML = copy.deepcopy(RML)
        
        C0 = np.zeros((len(feature_set), len(availableExperiments0),len(availableExperiments0)))
        C1 = np.zeros((len(feature_set), len(availableExperiments1),len(availableExperiments1)))

        for i in range(len(feature_set)):
            RML0[i],C0[i],theta0[i], sigma20[i], predOptProp0[i], sigma0[i], EI0[i], beta0[i],nugget0[i] = setup.evaluate_EI_averaging(gp0, priorKnowledge0[i], availableExperiments0, gpSamples0[i], criteria[0])
            RML1[i],C1[i],theta1[i], sigma21[i], predOptProp1[i], sigma1[i], EI1[i], beta1[i],nugget1[i] = setup.evaluate_EI_averaging(gp1, priorKnowledge1[i], availableExperiments1, gpSamples1[i], criteria[1])

            dummy_Y0 = availableExperiments0.reshape(len(availableExperiments0),1)-((np.std(availableExperiments0.reshape(1,-1))*beta0[i])+np.mean(availableExperiments0.reshape(1,-1)))
            hessian_det0[i] = setup.evaluate_hessian_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
            std_target = np.std(availableExperiments0)
            lap_coeff0[i] = setup.evaluate_laplace_coefficient(hessian_det0[i],theta0[i],np.sqrt(sigma20[i]), std_target)
            dummy_Y1 = availableExperiments1.reshape(len(availableExperiments0),1)-((np.std(availableExperiments1.reshape(1,-1))*beta1[i])+np.mean(availableExperiments1.reshape(1,-1)))
            hessian_det1[i] = setup.evaluate_hessian_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
            std_target = np.std(availableExperiments1)
            lap_coeff1[i] = setup.evaluate_laplace_coefficient(hessian_det1[i],theta1[i],np.sqrt(sigma21[i]), std_target)
            lap_coeff[i] = lap_coeff0[i] + lap_coeff1[i]
            RML0_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge0[i], dummy_Y0,theta0[i],np.sqrt(sigma20[i]),nugget0[i])
            RML1_tmp = setup.evaluate_log_likelihhod_old(priorKnowledge1[i], dummy_Y1,theta1[i],np.sqrt(sigma21[i]),nugget1[i])
            if RML0_tmp != np.inf:
                RML0[i] = RML0_tmp
            if RML1_tmp != np.inf:
                RML1[i] = RML1_tmp
            RML[i]=RML0[i]+RML1[i]
            RML_sum = RML_sum+RML[i]
            
        
        if np.isnan(lap_coeff[i]):
            print (old_lap_coeff[i])
            lap_coeff[i] = old_lap_coeff[i]
        if np.isnan(RML[i]):
             RML[i] = old_RML[i]
        
        eff_coeff =RML
        min_coeff =max(eff_coeff)
        log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
        coeffs = np.exp(eff_coeff - log_sum)
        EI_effective0= calculate_EI_effective(n_mat,EI0,coeffs)
        EI_effective1= calculate_EI_effective(n_mat,EI1,coeffs)
        
        Mean_0_effective0= calculate_EI_effective(n_mat,predOptProp0,coeffs)
        Mean_1_effective1= calculate_EI_effective(n_mat,predOptProp1,coeffs)

                # Assign GP predictions to materials properties
        for i in range(n_mat):
            materials[i]['EI_1'] = EI_effective0[i]
            materials[i]['Mean_1'] = Mean_0_effective0[i]
            materials[i]['EI_2'] = EI_effective1[i]
            materials[i]['Mean_2'] = Mean_1_effective1[i]
        
        candidate_P_1, candidate_P_2, new_candidate_list = pareto_frontier(availableExperiments0, availableExperiments1,maxX = True, maxY = False)
        k = len(candidate_P_1)
        if it==1:
            Pareto_K = [candidate_P_1[i][0] for i in range(k)]
            Pareto_G = [candidate_P_2[i][0] for i in range(k)]
        else:
            Pareto_K = candidate_P_1
            Pareto_G = candidate_P_2
        
        S = sorted([-candidate_P_1[i], candidate_P_2[i]] for i in range(k))
        r = [-50,200]

        ehvi = np.zeros((len(feature_set),n_mat))
        
        for iii in range(len(feature_set)):
            for i in range(n_mat):
                mu = [-predOptProp0[iii][i], predOptProp1[iii][i]]
                s = [sigma0[iii][i],sigma1[iii][i]]
                ehvi[iii][i] = exi2d(S,r,mu,s)
        
        EHVI_effective= calculate_EI_effective(n_mat,ehvi,coeffs)
        for i in range(n_mat):
            materials[i]['EHVI'] = EHVI_effective[i]

        # Assign EHVI to materials properties
                
        candidates = sorted(materials,key=itemgetter('EHVI'),reverse=True)
        index_list=[]
        for i in range(0,n_mat):
            index_list.append(candidates[i]['Index'])
        exclusion_list =[]
        for i in range(0,len(excludedList)):
            exclusion_list.append(excludedList[i])
        
        
        
        for l in index_list[:]:
            if l in exclusion_list:
                index_list.remove(l)
        ncandidates = 2
        candidate_index = index_list[:ncandidates]
        
        
        for i in range(0,len(candidate_index)):
            n = int(candidate_index[i])
            excludedList = np.append(excludedList, n)
            updated_prior0 = np.zeros((len(feature_set),len(excludedList),4))
            updated_prior1 = np.zeros((len(feature_set1),len(excludedList),4))
            for iiiii in range(len(feature_set)):
            
                currentFeatures0 = np.atleast_2d(gpSamples0[iiiii, n-1, :])
                currentFeatures1 = np.atleast_2d(gpSamples1[iiiii, n-1, :])
                a0 = np.insert(priorKnowledge0[iiiii], len(priorKnowledge0[iiiii]),currentFeatures0, axis=0)
                a1 = np.insert(priorKnowledge1[iiiii], len(priorKnowledge1[iiiii]),currentFeatures1, axis=0)
                updated_prior0[iiiii] = a0
                updated_prior1[iiiii] = a1
            
            
            priorKnowledge0 = updated_prior0
            priorKnowledge1 = updated_prior1
            list_data =[n] 
            materials[n-1]['K'] = materials[n-1]['K_exp']
            materials[n-1]['G'] = materials[n-1]['G_exp']
            materials[n-1]['E'] = materials[n-1]['E_exp']
            new_property = np.zeros((len(target_property),1, 1))
            new_property0 = materials[n-1]['K']
            new_property1 = materials[n-1]['G']
            availableExperiments0 = np.append(availableExperiments0,new_property0)
            availableExperiments1 = np.append(availableExperiments1,new_property1)

            candidate_material = materials[n-1]['MAX']
            time.sleep(2)
            calc_K = [x['K'] for x in materials]
            calc_G = [x['G'] for x in materials]

            ductility_index_calc = np.ones(len(calc_K))
            pred_D = predOptProp0*predOptProp1
            
        Pareto_points_found=list(set(excludedList).intersection(True_Pareto_list))
  
        for x in range(len(calc_G)):
           if calc_G[x] ==0:
                ductility_index_calc[x] = 0
           else:
                ductility_index_calc[x] = calc_K[x]/calc_G[x]
        D_max =  np.amax(ductility_index_calc)       
        Max_Index = materials[np.argmax(ductility_index_calc)]['Index']
        Max_MAX = materials[np.argmax(ductility_index_calc)]['MAX']
        K_max = materials[np.argmax(ductility_index_calc)]['K']
        G_min = materials[np.argmax(ductility_index_calc)]['G']

        ego_run_data = dict()
        ego_run_data.update({'initial_G':np.asarray(initial_G).tolist()})
        ego_run_data.update({'initial_K':np.asarray(initial_K).tolist()})
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EHVI':np.amax(ehvi)})
        ego_run_data.update({'Max_D_calc':np.amax(ductility_index_calc)})
        ego_run_data.update({'Number_ParetoPoints_Found':len(Pareto_points_found)})
	ego_run_data.update({'L_coeff':coeffs.tolist()})
        #print(len(Pareto_points_found))
        #print(np.shape(priorKnowledge0))
        #time.sleep(2)
        output_list.append(ego_run_data)
    materials = sorted(materials,key=itemgetter('Index'))    
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set),str(feature_type)))
#----------------------------------------------------------------------------------------------------%
#Method to take two equally-sized lists and return just the elements which lie 
#on the Pareto frontier, sorted into order.
#Default behaviour is to find the maximum for both X and Y, but the option is
#available to specify maxX = False or maxY = False to find the minimum for either
#or both of the parameters.

def pareto_frontier(Xs, Ys, maxX = True, maxY = True):
    index_list = np.linspace(1,len(Xs),len(Xs),dtype=int)
# Sort the list in either ascending or descending order of X
    myList = sorted([[Xs[i], Ys[i],index_list[i]] for i in range(len(Xs))], reverse=maxX)
# Start the Pareto frontier with the first value in the sorted list
    p_front = [myList[0]]
# Loop through the sorted list
    for pair in myList[1:]:
        if maxY:
            if pair[1] >= p_front[-1][1]: # Look for higher values of 
                p_front.append(pair) # and add them to the Pareto frontier
        else:
            if pair[1] <= p_front[-1][1]: # Look for lower values of 
                p_front.append(pair) # and add them to the Pareto frontier
# Turn resulting pairs back into a list of Xs and Ys
    p_frontX = [pair[0] for pair in p_front]
    p_frontY = [pair[1] for pair in p_front]
    p_index  = [pair[2] for pair in p_front]
    return p_frontX, p_frontY, p_index
#---------------------------------------------------------------------------------------------------------------------------------
def calculate_EI_effective(n_mat, EI_array, weights):
    EI_effective= np.zeros(n_mat)
    for j in range(len(weights)):
            EI_array[j] = EI_array[j]*weights[j]
            EI_effective = EI_effective + EI_array[j]   
    return EI_effective

#----------------------------------------------------------------------------------------------------------------------------
def gaussian_predictor_averaging_first_order(materials, target_property, feature_set, availExpCount, excludedList,criteria,gp_type,input_list, input_set,feature_type):   
    
    # define calculation parameters
   
    #stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters_direct()
    orig_materials = materials
    n_mat=len(materials)
    #----------------------------------------------------------------------------------------------------%
    priorKnowledge =np.zeros((len(feature_set),availExpCount,4))
    gpSamples=np.zeros((len(feature_set),n_mat,4))
    for i in range(len(feature_set)):
        priorKnowledge[i], availableExperiments, gpSamples[i] = setup.setup_gp(materials,availExpCount, target_property,feature_set[i],excludedList)
    gp = setup.initialize_gp(availableExperiments,gp_type)
    predOptProps = np.zeros((len(feature_set),n_mat))
    sigma = np.zeros((len(feature_set),n_mat))
    EI = np.zeros((len(feature_set),n_mat))
    RML =np.zeros(len(feature_set))
    RML_sum = 0
    C = np.zeros((len(feature_set), availExpCount,availExpCount))
    theta = np.zeros((len(feature_set),1))
    sigma2 = np.zeros((len(feature_set),1))
    nugget_ = np.zeros((len(feature_set),1))
    if gp_type == 'constant':
    	beta = np.zeros((len(feature_set),1))
	for i in range(len(feature_set)):
        	RML[i],C[i],theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
        	dummy_Y = availableExperiments-((np.std(availableExperiments)*beta[i])+np.mean(availableExperiments))
        	RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        	if RML_tmp != np.inf:
           		RML[i]=RML_tmp 
		RML_sum = RML_sum+RML[i]
    	eff_coeff =RML
    	min_coeff =max(eff_coeff)
    	log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
    	coeffs = np.exp(eff_coeff - log_sum)
    	EI_effective= calculate_EI_effective(n_mat,EI,coeffs)
    elif gp_type =='linear':
        beta = np.zeros((len(feature_set),5,1))    
    	for i in range(len(feature_set)):
        	RML[i],C[i],theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
          	dummy_Y = availableExperiments-((np.std(availableExperiments.reshape(1,-1))*np.dot(np.concatenate((np.ones((priorKnowledge[i].shape[0],1)),pre.scale(priorKnowledge[i])),axis=1),beta[i]))+np.mean(availableExperiments.reshape(1,-1)))
		RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        	if RML_tmp != np.inf:
	            	RML[i]=RML_tmp
		RML_sum = RML_sum+RML[i]
	eff_coeff =RML
	min_coeff =max(eff_coeff)
    	log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
    	coeffs = np.exp(eff_coeff - log_sum)
    	EI_effective= calculate_EI_effective(n_mat,EI,coeffs)

    #----------------------------------------------------------------------------------------------------%
    maxEI = np.amax(EI_effective) * np.ones(maxEICheck)
    
    it = 0
    numIter = 40
    output_list=[]  
    while not stopEILoop and it < numIter and len(excludedList) < len(materials):
        it+=1

        # sort materials back to its original order
        materials = sorted(materials,key=itemgetter('Index'))
        for i in range(len(materials)):
            materials[i]['EI'] = EI_effective[i]
        # Fitting GP
        for i in range(len(feature_set)):
            		RML[i],C, theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
			if gp_type == 'constant':
		                dummy_Y = availableExperiments.reshape(len(availableExperiments),1)-((np.std(availableExperiments.reshape(1,-1))*beta[i])+np.mean(availableExperiments.reshape(1,-1)))
			elif gp_type =='linear':
				dummy_Y = availableExperiments.reshape(len(availableExperiments),1) - ((np.std(availableExperiments.reshape(1,-1))*np.dot(np.concatenate((np.ones((priorKnowledge[i].shape[0],1)),pre.scale(priorKnowledge[i])),axis=1),beta[i]))+np.mean(availableExperiments.reshape(1,-1)))	        
    			RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i]) 
    
		        if RML_tmp != np.inf:
                		RML[i]=RML_tmp 
    
        eff_coeff =RML
        min_coeff =max(eff_coeff)
        log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
        coeffs = np.exp(eff_coeff - log_sum)
        EI_effective= calculate_EI_effective(n_mat,EI,coeffs) 

        maxEI = np.roll(maxEI, -1)
        maxEI[-1] = np.amax(EI_effective)
     

        # Assign GP predictions to materials properties
        for j in range(len(materials)):
            materials[j]['EI'] = EI_effective[j]
 
        candidates = sorted(materials,key=itemgetter('EI'),reverse=True)
        tolDifferenceInFeatures = 20./100.
        tolReductionInEI = 100./100.
        smallDifferenceInFeatures = False
        candidate_Index = []
        m = 0
        iStop = 0
        while(iStop < num_candidates and m < len(materials) and not stopEILoop):

           currentFeatures =np.zeros((len(feature_set),1,4))
           for i in range(len(feature_set)):
                currentFeatures[i] = gpSamples[i][candidates[m]['Index']-1]
           if candidates[m]['Index'] not in excludedList:# and smallDifferenceInFeatures == False:
                candidate_Index = (candidates[m]['Index'])
                excludedList = np.append(excludedList, candidate_Index)
                updated_prior = np.zeros((len(feature_set),len(excludedList),4))
                for i in range(len(feature_set)):
                    a = np.insert(priorKnowledge[i], len(priorKnowledge[i]),currentFeatures[i], axis=0)
                    updated_prior[i] = a
                if criteria =='min':
                    materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['G_exp']
                elif criteria =='max':
                    materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['K_exp']
                availableExperiments = np.append(availableExperiments,materials[candidate_Index-1][target_property[0]])
                candidate_material = candidates[m]['MAX']
                priorKnowledge = updated_prior
                iStop+=1
           m+=1     
        calc_K = [x['K'] for x in materials]
        calc_G = [x['G'] for x in materials]
        max_K = max(availableExperiments)
        min_G = min(availableExperiments)
        ego_run_data = dict()
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EI':np.max(EI_effective)})
        ego_run_data.update({'max_Bulk_Calculated':max_K})
        ego_run_data.update({'min_G_Calculated':min_G})
	ego_run_data.update({'L_coeff':coeffs.tolist()})
        output_list.append(ego_run_data)
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set), str(feature_type)))

 
#----------------------------------------------------------------------------------------------------------------------------
def gaussian_predictor_averaging_second_order(materials, target_property, feature_set, availExpCount, excludedList,criteria,gp_type,input_list, input_set,feature_type):   
    
    # define calculation parameters
    #stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters(params_vasp)
    stopEI, stopEILoop, numIter, num_candidates, maxEICheck = setup.setup_predictor_calculation_parameters_direct()
    orig_materials = materials
    n_mat=len(materials)
    #----------------------------------------------------------------------------------------------------%
    num_candidates=2
    priorKnowledge =np.zeros((len(feature_set),availExpCount,4))
    gpSamples=np.zeros((len(feature_set),n_mat,4))
    for i in range(len(feature_set)):
        priorKnowledge[i], availableExperiments, gpSamples[i] = setup.setup_gp(materials,availExpCount, target_property,feature_set[i],excludedList)
    gp = setup.initialize_gp(availableExperiments,gp_type)
    predOptProps = np.zeros((len(feature_set),n_mat))
    sigma = np.zeros((len(feature_set),n_mat))
    EI = np.zeros((len(feature_set),n_mat))
    RML =np.zeros(len(feature_set))
    hessian_det = np.zeros((len(feature_set),2,2))
    lap_coeff = np.zeros(len(feature_set))
    C = np.zeros((len(feature_set), availExpCount,availExpCount))
    theta = np.zeros((len(feature_set),1))
    sigma2 = np.zeros((len(feature_set),1))
#    beta = np.zeros((len(feature_set),1))
    nugget_ = np.zeros((len(feature_set),1))
    if gp_type == 'constant':
        beta = np.zeros((len(feature_set),1))
   
	for i in range(len(feature_set)):
        
        	RML[i],C[i],theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
        	dummy_Y = availableExperiments-((np.std(availableExperiments)*beta[i])+np.mean(availableExperiments))
        	hessian_det[i] = setup.evaluate_hessian_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        	std_target = np.std(availableExperiments)
        	lap_coeff[i] = setup.evaluate_laplace_coefficient(hessian_det[i],theta[i],np.sqrt(sigma2[i]), std_target)
              
        	RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        	if RML_tmp != np.inf:
           		RML[i]=RML_tmp
    if gp_type =='linear':
	beta = np.zeros((len(feature_set),5,1))
     	RML[i],C[i],theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
        dummy_Y = availableExperiments-((np.std(availableExperiments.reshape(1,-1))*np.dot(np.concatenate((np.ones((priorKnowledge[i].shape[0],1)),pre.scale(priorKnowledge[i])),axis=1),beta[i]))+np.mean(availableExperiments.reshape(1,-1)))
        hessian_det[i] = setup.evaluate_hessian_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        std_target = np.std(availableExperiments)
        lap_coeff[i] = setup.evaluate_laplace_coefficient(hessian_det[i],theta[i],np.sqrt(sigma2[i]), std_target)
                
        RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
        if RML_tmp != np.inf:
           RML[i]=RML_tmp

    eff_coeff = RML+ lap_coeff
    min_coeff = max(eff_coeff)
    log_sum = min_coeff + np.log(np.sum(np.exp(eff_coeff-min_coeff)))
    coeffs = np.exp(eff_coeff - log_sum)

    EI_effective= calculate_EI_effective(n_mat,EI, coeffs)
    #----------------------------------------------------------------------------------------------------%
    maxEI = np.amax(EI_effective) * np.ones(maxEICheck)
    
    it = 0
    numIter = 40
    output_list=[]  
    max_K = max(availableExperiments)
    min_G = min(availableExperiments)
    ego_run_data = dict()
    ego_run_data.update({'Iteration':it})
    ego_run_data.update({'MAX_EI':np.max(EI_effective)})
    ego_run_data.update({'max_Bulk_Calculated':max_K[0]})
    ego_run_data.update({'min_G_Calculated':min_G[0]})
    output_list.append(ego_run_data)
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set), str(feature_type)))

    while not stopEILoop and it < numIter and len(excludedList) < len(materials) and max_K < 300:
        it+=1
        old_lap_coeff = copy.deepcopy(lap_coeff)
        old_RML = copy.deepcopy(RML)
        # sort materials back to its original order
        materials = sorted(materials,key=itemgetter('Index'))
        for i in range(len(materials)):
            materials[i]['EI'] = EI_effective[i]
        # Fitting GP    
        C = np.zeros((len(feature_set), len(availableExperiments),len(availableExperiments)))
        for i in range(len(feature_set)):
            RML[i],C[i],theta[i], sigma2[i], predOptProps[i], sigma[i], EI[i],beta[i],nugget_[i] = setup.evaluate_EI_averaging(gp, priorKnowledge[i], availableExperiments, gpSamples[i], criteria)
	    if gp_type == 'constant':
	            dummy_Y = availableExperiments.reshape(len(availableExperiments),1)-((np.std(availableExperiments.reshape(1,-1))*beta[i])+np.mean(availableExperiments.reshape(1,-1)))
            elif gp_type =='linear':
				dummy_Y = availableExperiments.reshape(len(availableExperiments),1) - ((np.std(availableExperiments.reshape(1,-1))*np.dot(np.concatenate((np.ones((priorKnowledge[i].shape[0],1)),pre.scale(priorKnowledge[i])),axis=1),beta[i]))+np.mean(availableExperiments.reshape(1,-1)))	        
    	    RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i]) 
	    hessian_det[i] = setup.evaluate_hessian_old(priorKnowledge[i], dummy_Y,theta[i],np.sqrt(sigma2[i]),nugget_[i])
            std_target = np.std(availableExperiments.reshape(1,-1))
            lap_coeff[i] = setup.evaluate_laplace_coefficient(hessian_det[i],theta[i],np.sqrt(sigma2[i]), std_target)  
                   
            RML_tmp=setup.evaluate_log_likelihhod_old(priorKnowledge[i], dummy_Y ,theta[i],np.sqrt(sigma2[i]),nugget_[i])
            if RML_tmp != np.inf:
                RML[i]=RML_tmp 
        if np.isnan(lap_coeff[i]):
            print (old_lap_coeff[i])
            lap_coeff[i] = old_lap_coeff[i]
        if np.isnan(RML[i]):
             RML[i] = old_RML[i]
        eff_coeff = RML + lap_coeff

        min_coeff = max(eff_coeff)
        log_sum = min_coeff + np.log(np.sum(np.exp(np.longfloat(eff_coeff-min_coeff))))
        coeffs = np.exp(eff_coeff - log_sum)
        EI_effective= calculate_EI_effective(n_mat,EI, coeffs)

        maxEI = np.roll(maxEI, -1)
        maxEI[-1] = np.amax(EI_effective)
     

        # Assign GP predictions to materials properties
        for j in range(len(materials)):
            materials[j]['EI'] = EI_effective[j]
 
        candidates = sorted(materials,key=itemgetter('EI'),reverse=True)
        tolDifferenceInFeatures = 20./100.
        tolReductionInEI = 100./100.
        smallDifferenceInFeatures = False
        candidate_Index = []
        m = 0
        iStop = 0
        while(iStop < num_candidates and m < len(materials) and not stopEILoop):

           currentFeatures =np.zeros((len(feature_set),1,4))
           for i in range(len(feature_set)):
                currentFeatures[i] = gpSamples[i][candidates[m]['Index']-1]
        

           if candidates[m]['Index'] not in excludedList:# and smallDifferenceInFeatures == False:
              candidate_Index = (candidates[m]['Index'])
              excludedList = np.append(excludedList, candidate_Index)
    
              updated_prior = np.zeros((len(feature_set),len(excludedList),4))
   
  
 
              for i in range(len(feature_set)):
                    a = np.insert(priorKnowledge[i], len(priorKnowledge[i]),currentFeatures[i], axis=0)
                    updated_prior[i] = a
              if criteria =='min':
                     materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['G_exp']
              elif criteria =='max':
                        materials[candidate_Index-1][target_property[0]] = materials[candidate_Index-1]['K_exp']
              availableExperiments = np.append(availableExperiments,materials[candidate_Index-1][target_property[0]])

              candidate_material = candidates[m]['MAX']
              priorKnowledge = updated_prior

              iStop+=1
           m+=1     
        
        calc_K = [x['K'] for x in materials]
        calc_G = [x['G'] for x in materials]
        max_K = max(availableExperiments)
        min_G = min(availableExperiments)
        ego_run_data = dict()
        ego_run_data.update({'Iteration':it})
        ego_run_data.update({'MAX_EI':np.max(EI_effective)})
        if criteria == 'max':
            ego_run_data.update({'max_Bulk_Calculated':max_K})
        elif criteria == 'min':
            ego_run_data.update({'min_G_Calculated':min_G})
        ego_run_data.update({'L_coeff':coeffs.tolist()})
        output_list.append(ego_run_data)
    json_coders.json_pretty_dump(output_list,'{}_{}_{}_ego_run_data.json'.format(str(input_list), str(input_set), str(feature_type)))
             
    #----------------------------------------------------------------------------------------------------%
#----------------------------------------------------------------------------------------------------%
