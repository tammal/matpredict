#! /usr/bin/env python
"""Module for running and control of EGO
"""

__author__ = "Anjana Talapatra (anjanatalapatra@tamu.edu)"
__version__ = "0.1$"
__date__ = "$Date: May 2016 $"
__copyright__ = "Copyright (c) 2016 Anjana Talapatra"
__license__ = "Python"

from csv import DictReader
from json import load, dump
from gaussian_predictor import gaussian_predictor, gaussian_predictor_averaging_first_order


def main():

	jsonfile = 'MAX_data.json'
	feature =['average']
	criteria = 'max'
	run_type ='single'
	target_property = ['K']
	knowledge_list =[5,17]
	input_list=('N_2')
	input_set=1
	gp_type='constant'
	feature_set_no =1
	materials = load(open(jsonfile))
	availExpCount = len(knowledge_list)
#-----------for single model-----------------------------------------------------------------------------------------------------------------------------------------#
	#feature_set =[]
	#feature_set=['Cv','m','C','Type']
	#gaussian_predictor(materials, target_property, feature_set, availExpCount, knowledge_list,criteria, gp_type, input_list[0], input_set,feature_set_no)
	
#-----------for BMA-----------------------------------------------------------------------------------------------------------------------------------------#

	feature_set=[]
	feature_set.append(['C','Cv','m','c_exp'])
	feature_set.append(['m','Z','I_dist','e_a'])

	#
	gaussian_predictor_averaging_first_order(materials, target_property, feature_set, availExpCount, knowledge_list,criteria,gp_type,input_list, input_set,feature[0])
#-----------for BMA-----------------------------------------------------------------------------------------------------------------------------------------#	
	
if __name__ == '__main__':
	main()		
		
		
