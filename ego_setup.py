#! /usr/bin/env python
"""
Fuctions to set up EGO calculations, BMA
"""

__author__ = "Anjana Talapatra (anjanatalapatra@tamu.edu),Thien Duong, Shahin Boluki "
__version__ = "0.1$"
__date__ = "$Date: May 2016 $"
__copyright__ = "Copyright (c) 2016 Anjana Talapatra, Thien Duong, Shahin Boluki"
__license__ = "Python"

#----------------------------------------------------------------------------------------------------%
import os
import sys
import json
import numpy as np
import scipy as sp
from operator import itemgetter
import time
import json
from sklearn.gaussian_process import GaussianProcess
import sklearn.preprocessing as pre
import sklearn.metrics.pairwise as pair
from ehvi import exi2d
from scipy.linalg import cholesky, cho_solve, solve_triangular
import random

#----------------------------------------------------------------------------------------------------%
def load_input_data(filename):
    materials = json.load(open(filename))
    print ('Available properties of materials:' )
    print (materials[0])
#----------------------------------------------------------------------------------------------------%
def get_selective_features(materials, target_property):
    availExpCount = 0
    excludedList = []
    for iMater in range(len(materials)):
        for iOptProp in range(len(target_property)):
            if materials[iMater][target_property[iOptProp]] == 0:
                goodExp = False
                break
            else:
                goodExp = True
        if goodExp == True:
            availExpCount += 1
            excludedList = np.append(excludedList, materials[iMater]['Index'])
    excludedList = excludedList.astype(int)
    return availExpCount, excludedList
#----------------------------------------------------------------------------------------------------%
def setup_gp(materials,availExpCount, target_property, feature_set, excludedList):
    priorKnowledge = np.zeros((availExpCount, len(feature_set)))
    availableExperiments = np.zeros((availExpCount, len(target_property)))
    for i in range(len(excludedList)):
        iMater = excludedList[i]-1
        iFeature = 0
        iOptPro = 0
        for feature in feature_set:
            priorKnowledge[i, iFeature] = materials[iMater][feature]
            iFeature+=1
        for optProp in target_property: 
            availableExperiments[i, iOptPro] = materials[iMater][optProp]
            iOptPro+=1
    
    gpSamples = np.zeros((len(materials), len(feature_set)))
    for iMater in range(len(materials)):
        iFeature = 0
        for feature in feature_set:

                gpSamples[iMater, iFeature] = materials[iMater][feature]
                iFeature+=1    
    return priorKnowledge, availableExperiments, gpSamples

#----------------------------------------------------------------------------------------------------%

def initialize_gp(availableExperiments, gp_type ='constant'):
    if gp_type == 'constant':
	gp = GaussianProcess(regr='constant',corr='squared_exponential', thetaL=1e-4, thetaU=1e-1,random_start=100)
    elif gp_type == 'linear':
        gp = GaussianProcess(regr='linear',corr='squared_exponential', thetaL=1e-4, thetaU=1e-1,random_start=100)
    return gp
#------------------------------------------------------------------------------------------------ 
def evaluate_EI(gp, priorKnowledge, availableExperiments,gpSamples,criteria='max'):
    gp.fit(priorKnowledge, availableExperiments)
    predOptProps, MSE = gp.predict(gpSamples, eval_MSE=True)
    predOptProps = predOptProps.ravel()
    MSE = np.reshape(MSE, np.shape(predOptProps))
    sigma = np.sqrt(MSE)
    if criteria =='max':
        bestValue = np.amax(availableExperiments, axis=0)
        err=(predOptProps-bestValue)/sigma
    elif criteria =='min':
        bestValue = np.amin(availableExperiments, axis=0)
        err=(bestValue-predOptProps)/sigma   
    EI=sigma*(sp.stats.norm.pdf(err) + err*sp.stats.norm.cdf(err))

    return predOptProps, sigma, EI

#----------------------------------------------------------------------------------------------------%
def evaluate_EI_averaging(gp, priorKnowledge, availableExperiments,gpSamples,criteria='max'):
    fit = gp.fit(priorKnowledge, availableExperiments)
    theta = fit.theta_
    predOptProps, MSE = gp.predict(gpSamples, eval_MSE=True)
    predOptProps = predOptProps.ravel()
    MSE = np.reshape(MSE, np.shape(predOptProps))
    sigma = np.sqrt(MSE)
    if criteria =='max':
        bestValue = np.amax(availableExperiments, axis=0)
        err=(predOptProps-bestValue)/sigma
    elif criteria =='min':
        bestValue = np.amin(availableExperiments, axis=0)
        err=(bestValue-predOptProps)/sigma
    EI=sigma*(sp.stats.norm.pdf(err) + err*sp.stats.norm.cdf(err))
    RML = fit.reduced_likelihood_function()[0]
    C=np.dot(fit.C,fit.C.conj().T)
    sigma2 = fit.sigma2
    beta = fit.beta
    nuggt = fit.nugget

    return RML,C, theta, sigma2, predOptProps, sigma, EI,beta,nuggt

#-------------------------------------------------------------------------
def evaluate_EI_averaging_woutB(gp, priorKnowledge, availableExperiments,gpSamples,criteria='max'):
    fit = gp.fit(priorKnowledge, availableExperiments)
    theta = fit.theta_
    predOptProps, MSE = gp.predict(gpSamples, eval_MSE=True)
    predOptProps = predOptProps.ravel()
    MSE = np.reshape(MSE, np.shape(predOptProps))
    sigma = np.sqrt(MSE)
    if criteria =='max':
        bestValue = np.amax(availableExperiments, axis=0)
        err=(predOptProps-bestValue)/sigma
    elif criteria =='min':
        bestValue = np.amin(availableExperiments, axis=0)
        err=(bestValue-predOptProps)/sigma
    EI=sigma*(sp.stats.norm.pdf(err) + err*sp.stats.norm.cdf(err))
    RML = fit.reduced_likelihood_function()[0]
    C=np.dot(fit.C,fit.C.conj().T)
    sigma2 = fit.sigma2
    beta = fit.beta
    nuggt = fit.nugget

    return RML,C, theta, sigma2, predOptProps, sigma, EI,nuggt

#----------------------------------------------------------------------------------------------------%
def evaluate_hessian(priorKnowledge, availableExperiments,theta,sigma2):
    X = pre.scale(priorKnowledge)
    nn = len(X)
    f = sigma2
    L= theta
    y = availableExperiments-np.mean(availableExperiments)
    K = np.zeros((nn,nn))
    K_1 = np.zeros((nn,nn))
    D2 = np.zeros((nn,nn))
    f_zegond = np.zeros((2,2))
    for i in range(0,nn):
        for j in range(0,i+1):
            K[i][j] = f**2*np.exp((-0.5)*(np.dot(X[i]-X[j],X[i]-X[j]))/L**2)
            D2[i][j] = np.dot(X[i]-X[j],X[i]-X[j])
            if i!=j:
                K[j][i] = K[i][j]
                D2[j][i] = D2[i][j]
    if np.linalg.cond(K) > 0:
   
            K_1 =np.linalg.inv(K)            
            alp = np.linalg.solve(K,y)  
            d_m = np.ones((2,nn,nn))
            dd_m = np.ones((2,2,nn,nn))
            d_m[1] = K*2/f
            d_m[0] =  D2/(L**3)*K
            dd_m[1][1] = K*2/(f*f)
            dd_m[0][1] = K*2*D2/(f*L*L*L)
            dd_m[1][0] = dd_m[0][1]
            dd_m[0][0] = K*(((-3)*((D2)/(L**4))) + (((D2)/(L**3))**2))

            for i in range(0,2):
                for j in range(0,i+1):
                    C = np.dot( (np.outer(alp,alp) - K_1), dd_m[i][j] )
                    D = np.dot(np.dot(K_1, d_m[j]), K_1)
                    A = np.outer( np.dot(-D, y), alp )
                    A = A + A.T
                    Cp = np.dot( (A+D), d_m[i] )
                    f_zegond[i][j] = 0.5*np.trace( Cp + C )
                    if i!=j:
                        f_zegond[j][i] = f_zegond[i][j]
            hessian_det = f_zegond
    return hessian_det
#----------------------------------------------------------------------------------------------------%
def evaluate_laplace_coefficient(hessian_det,theta,sigma2, std_target):
    hessian_det = -hessian_det
    from scipy.integrate import dblquad    
    func = lambda y,x: np.exp(-   ( (hessian_det[0,0]*( (x-theta)**2)) +  (hessian_det[1,1]*((y-sigma2)**2)) + (2*hessian_det[0,1]*((y-sigma2)*(x-theta))) )   /2)
    x1,x2 = (0.0001), (0.1)
    y1,y2 = lambda x: (0.8*sigma2), lambda x: (1.2*sigma2)
    laplace_coefficient = np.log(dblquad( func, x1, x2, y1, y2)[0])
    if np.isnan(laplace_coefficient) or np.isinf(laplace_coefficient):
        laplace_coefficient = -400
    return laplace_coefficient
#----------------------------------------------------------------------------------------------------%
def pearson_correlation(excludedList,gpSamples):
    corr, p = pearsonr(x,y)

#----------------------------------------------------------------------------------------------------%

def setup_predictor_calculation_parameters(params_vasp):

    stopEI = params_vasp['ego_stopEI'] 
    stopEILoop = params_vasp['ego_stopEILoop'] 
    numIter = params_vasp['ego_numIter']
    num_candidates = params_vasp['ego_num_candidates']
    maxEICheck = params_vasp['ego_maxEICheck']

    return stopEI, stopEILoop, numIter, num_candidates, maxEICheck

#-----------------------------------------------------------------------
def setup_predictor_calculation_parameters_direct():

    stopEI = 0.00001
    stopEILoop = False
    numIter = 20
    num_candidates = 2
    maxEICheck = 5
    return stopEI, stopEILoop, numIter, num_candidates, maxEICheck

#----------------------------------------------------------------------------------------------------%

def calculate_ehvi(pareto_front, reference, mean_i, sigma_i):
    ehvi = eng.eval("exi2d(pareto_front,reference,mean_i,sigma_i);",nargout=1)

    return ehvi

#----------------------------------------------------------------------------------------------------%
def evaluate_hessian_old(priorKnowledge, availableExperiments,theta,sigma2,nugg):
    X = pre.scale(priorKnowledge)
    nn = len(X)
    f = sigma2
    L= theta
    y = availableExperiments
    K = np.zeros((nn,nn))
    K_1 = np.zeros((nn,nn))
    D2 = np.zeros((nn,nn))
    f_zegond = np.zeros((2,2))
    D2= pair.euclidean_distances(X)**2
    K=  f**2*np.exp((- L)*(D2))
    K=K+(np.eye(nn)*nugg)
    if np.linalg.cond(K) > 0:
   
            K_1 =np.linalg.inv(K)
            alp = np.linalg.solve(K,y)  
            d_m = np.ones((2,nn,nn))
            dd_m = np.ones((2,2,nn,nn))
            d_m[1] = K*2/f
            d_m[0] =  (-D2)*K
            dd_m[1][1] = K*2/(f*f)
            dd_m[0][1] = K*(-2)*D2/f
            dd_m[1][0] = dd_m[0][1]
            dd_m[0][0] = D2*D2*K

            for i in range(0,2):
                for j in range(0,i+1):
                    C = np.dot( (np.outer(alp,alp) - K_1), dd_m[i][j] )
                    D = np.dot(np.dot(K_1, d_m[j]), K_1)
                    A = np.outer( np.dot(-D, y), alp )
                    A = A + A.T
                    Cp = np.dot( (A+D), d_m[i] )
                    f_zegond[i][j] = 0.5*np.trace( Cp + C )
                    if i!=j:
                        f_zegond[j][i] = f_zegond[i][j]
            hessian_det = f_zegond
    return hessian_det
#----------------------------------------------------------------------------------------------------%
def evaluate_log_likelihhod_old(priorKnowledge, availableExperiments,theta,sigma2,nugg):
    X = pre.scale(priorKnowledge)
    nn = len(X)
    f = sigma2
    L= theta
    y = availableExperiments
    D2= pair.euclidean_distances(X)**2
    K=  f**2*np.exp((- L)*(D2))                           
    K=K+(np.eye(nn)*nugg)
    log_ml = np.inf
    try:
        L = cholesky(K, lower=True)  # Line 2
    except np.linalg.LinAlgError:
        return (np.inf)
    alpha = cho_solve((L, True), y)  # Line 3

        # Compute log-likelihood (compare line 7)
    log_likelihood_dims = -0.5 * np.einsum("ik,ik->k", y, alpha)
    log_likelihood_dims -= np.log(np.diag(L)).sum()
    log_likelihood_dims -= K.shape[0] / 2 * np.log(2 * np.pi)
    log_ml = log_likelihood_dims.sum(-1)  # sum over dimensions
    return log_ml
